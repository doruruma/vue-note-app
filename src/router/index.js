import Vue from 'vue'
import VueRouter from 'vue-router'
import JwtService from '@/core/JwtService'

// Auth View
import SignIn from '../views/SignIn.vue'
import Register from '../views/Register.vue'

// Home
import Home from '../views/Home.vue'

// Notes View
import Notes from '../views/Notes.vue'
import CreateNote from '../views/CreateNote.vue'
import NoteDetail from '../views/NoteDetail.vue'

// Profile View
import Profile from '../views/Profile.vue'
import ChangePassword from '../views/ChangePassword.vue'
import ChangeProfile from '../views/ChangeProfile.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/sign-in',
    name: 'SignIn',
    component: SignIn,
    meta: {
      auth: false,
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      auth: false,
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      auth: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      auth: true
    }
  },
  {
    path: '/change-password',
    name: 'ChangePassword',
    component: ChangePassword,
    meta: {
      auth: true
    }
  },
  {
    path: '/change-profile',
    name: 'ChangeProfile',
    component: ChangeProfile,
    meta: {
      auth: true
    }
  },
  {
    path: '/notes',
    name: 'Notes',
    component: Notes,
    meta: {
      auth: true
    }
  },
  {
    path: '/note/:id',
    name: 'NoteDetail',
    component: NoteDetail,
    meta: {
      auth: true
    }
  },
  {
    path: '/create-note',
    name: 'CreateNote',
    component: CreateNote,
    meta: {
      auth: true
    }
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// Middleware
router.beforeEach((to, from, next) => {

  if (to.matched.some(record => record.meta.auth)) {    // if Meta Auth == true
    if (JwtService.getToken() == null) {
      router.push({ name: 'SignIn' })
    } else {
      next()
    }
  } else {
    if (JwtService.getToken() != null) {
      router.push({ name: 'Home' })
    } else {
      next()
    }
  }

})

export default router
