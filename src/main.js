import Vue from 'vue'
import App from './App.vue'
import router from './router'

import api from '@/core/ApiService'

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'animate.css/animate.min.css'
import './assets/css/main.css'

api.init()
api.setHeader()

Vue.use(VueToast)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

Vue.config.productionTip = false

// API Image URL
window.imgUrl = 'http://localhost:8080/images'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
